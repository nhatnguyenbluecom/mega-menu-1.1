<?php

namespace Isobar\Megamenu\Model\ResourceModel;


class Rootmenu extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    const TABLE_NAME = 'isobar_root_mega_menu';
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('isobar_root_mega_menu', 'id');
    }

    /**
     * Get root id by store id
     *
     * @param int $storeId
     * @return int|false
     */
    public function getRootIdByStoreId($storeId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getTable(self::TABLE_NAME), 'id')
                    ->where('store_id = :store_id')
                    ->where('as_root_menu = :as_root_menu');

        $bind = [':store_id' => $storeId, ':as_root_menu' => 1,];

        return $connection->fetchOne($select, $bind);
    }
}