<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Isobar\Megamenu\Block\Html;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Theme\Block\Html\Topmenu;
use Magento\Cms\Model\BlockRepository;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Registry;
/**
 * Html page top menu block
 */
class Topmega extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'Isobar_Megamenu::html/top_megamenu.phtml';

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    protected $rootMenuRepository;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Api\SortOrder
     */

    /**
     * @var \Magento\Framework\Api\SortOrder
     */
    protected $sortOrder;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    protected $megaMenuOptions;
    protected $storeManager;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Isobar\Megamenu\Api\RootmenuRepositoryInterface $rootMenuRepository,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrder $sortOrder,
        \Isobar\Megamenu\Model\Admin\Config\Source\Options $megaMenuOptions,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
    
        array $layoutProcessors = [],
        array $data = []
    )
    {
        $this->rootMenuRepository = $rootMenuRepository;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->megaMenuOptions = $megaMenuOptions;
        $this->megaMenuFactory = $megaMenuFactory;
        $this->megaMenuRepository = $megaMenuRepository;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        parent::__construct($context, $data);
    }

    public function getMegaMenuHtml()
    {
        $html = '';
        $filterGroups = [];
        $rootMenuId = $this->getRootMenuFromCurrentStore();
        if ($rootMenuId) {
            $filter = $this->filterBuilder
                ->create()
                ->setField('store_id')
                ->setValue($rootMenuId)
                ->setConditionType('eq');

            $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();
            $filterGroups = [$filterGroup];
            $searchCriteria = $this->searchCriteriaBuilder
                ->setFilterGroups($filterGroups)
                ->create();
            $sortOrder = $this->sortOrder->setField('sort')->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC);
            $searchCriteria->setSortOrders([$sortOrder]);
            $result = $this->megaMenuRepository->getList($searchCriteria);
            $items = $result->getItems();

            $treeData = $this->getMenuItemTreeData($items);
            $html = $this->buildHierachy(0, $treeData);
        }
        return $html;
    }

    /**
     * Get megamenu item to tree data
     * @param $items
     * @return array
     */
    public function getMenuItemTreeData($items)
    {
        $menuItemData = array(
            'items' => array(),
            'parents' => array()
        );
        foreach ($items as $menuItem) {
            $menuItemData ['items'] [$menuItem->getId()] = $menuItem;
            $menuItemData ['parents'] [$menuItem->getParentId()] [] = $menuItem->getId();
        }
        return $menuItemData;
    }

    /**
     * Prepare options data
     * @param $parentId
     * @param $treeData
     * @param int $level
     */
    public function buildHierachy($parentId, $treeData, $level = -1)
    {
        $html = '';
        $level ++;
        if (isset($treeData['parents'][$parentId])) {
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $html .= '<li><a href="' .$treeData['items'][$itemId]->getLink(). '">'. $treeData['items'][$itemId]->getTitle() .'</a>';
                if (in_array($itemId, $treeData['parents'][$parentId])) {
                    $html .= '<ul>'. $this->buildHierachy($itemId, $treeData, $level) . '</ul>';
                }
                $html .= '</li>';
            }
        }
        return $html;
    }

    public function getRootMenuFromCurrentStore()
    {
        $currentStoreId = $this->storeManager->getStore()->getId();
        $rootMenuId = $this->rootMenuRepository->getRootIdByStoreId($currentStoreId);
        return $rootMenuId;
    }
}
