<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;
use Isobar\Megamenu\Model\Megamenu;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Magento\Framework\File\UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @var \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory
     */
    protected $megaMenuFactory;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;


    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Filesystem $filesystem,
        DataPersistorInterface $dataPersistor,
        \Isobar\Megamenu\Api\Data\MegamenuInterfaceFactory $megaMenuFactory,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->megaMenuFactory = $megaMenuFactory;
        $this->megaMenuRepository = $megaMenuRepository;
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($context, $coreRegistry);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');

            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Megamenu::STATUS_ENABLED;
            }
            if (empty($data['id'])) {
                $data['id'] = null;
            }

            try {
                $item = $this->megaMenuFactory->create();
                $data['created_at'] = (new \DateTime())->getTimestamp();
                $item->setData($data);
                $model = $this->megaMenuRepository->save($item);
                if (!$model->getId() && $id) {
                    $this->messageManager->addError(__('This menu item no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
                $this->messageManager->addSuccess(__('You saved the menu item.'));
                $this->dataPersistor->clear('isobar_megamenu');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['root_id' => $data['root_id'], 'id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/rootmenu/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                echo $e->getMessage();die();
                $this->messageManager->addException($e, __('Something went wrong while saving the menu item.'));
            }

            $this->dataPersistor->set('isobar_megamenu', $data);
            return $resultRedirect->setPath('*/*/edit', ['root_id' => $data['root_id'], 'id' => $model->getId()]);
        }
        return $resultRedirect->setPath('*/rootmenu/');
    }

}
