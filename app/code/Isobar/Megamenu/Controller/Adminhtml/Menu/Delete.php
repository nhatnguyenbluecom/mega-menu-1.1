<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Menu;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    /**
     * Delete constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
    ) {
        $this->megaMenuRepository = $megaMenuRepository;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                // init model and delete
                $this->megaMenuRepository->deleteById($id);
                // display success message
                $this->messageManager->addSuccess(__('You deleted the mega menu.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a mega menu to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
